const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send(`
    <form action="/submit" method="post">
      <label for="username">Username:</label>
      <input type="text" id="username" name="username"><br><br>
      <label for="email">Email:</label>
      <input type="email" id="email" name="email"><br><br>
      <button type="submit">Submit</button>
    </form>
  `);
});

app.post('/submit', (req, res) => {
  const { username, email } = req.body;

  // Read existing users data
  let users = [];
  try {
    users = JSON.parse(fs.readFileSync('users.json'));
  } catch (error) {
    console.error("Error reading users.json:", error);
  }

  // Add new user
  users.push({ username, email });

  // Write updated users data back to file
  fs.writeFile('users.json', JSON.stringify(users, null, 2), (err) => {
    if (err) {
      console.error("Error writing to users.json:", err);
      res.status(500).send('Error occurred while saving user data.');
    } else {
      console.log("User data saved successfully.");
      res.send(`User ${username} with email ${email} added successfully!`);
    }
  });
});

app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});
